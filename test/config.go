package test

import (
	"encoding/json"
	"io/ioutil"
)

var cfg Configuration

type Configuration struct {
	data map[string]interface{}
}

func New(fullpath string) (*Configuration, error) {
	b, err := LoadFile(fullpath)
	if err != nil {
		return nil, err
	}

	err = LoadByets(b, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}

func LoadFile(fullpath string) ([]byte, error) {
	f, err := ioutil.ReadFile(fullpath)
	if err != nil {
		return f, err
	}
	return f, nil
}

func LoadByets(d []byte, c *Configuration) error {
	err := json.Unmarshal(d, c.data)
	if err != nil {
		return err
	}
	return nil
}
