package test

import (
	"simplerest/config"
	"testing"
)

func TestLoadFile(t *testing.T) {
	_, err := config.LoadFile("config.json")
	if err != nil {
		t.ErrorF("No se pudo leer el archivo: %v", err)
	}
}
